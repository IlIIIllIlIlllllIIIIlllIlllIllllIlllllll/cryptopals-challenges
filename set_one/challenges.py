import base64
import pprint


def challenge_one(input_hex):
    decoded_hex = bytes.fromhex(input_hex)  # gets hexadecimal string byte data
    base64_encoded_hex = base64.b64encode(decoded_hex)  # base64 encodes hexadecimal string byte data
    return base64_encoded_hex.decode("utf-8")  # returns string representation of base64 encoded data


def challenge_two(buffer_one, buffer_two):
    decoded_buffer_one = bytes.fromhex(buffer_one)  # gets hexadecimal string byte data
    decoded_buffer_two = bytes.fromhex(buffer_two)  # gets hexadecimal string byte data

    xord_buffers = []
    for i in range(0, len(decoded_buffer_one)):  # for each byte in both buffers
        byte = decoded_buffer_one[i] ^ decoded_buffer_two[i]  # get res byte by xor'ing byte of buffer one and two
        hexd_byte = hex(byte)[2:]  # 2: removes 0x prefix on hex bytes
        xord_buffers.append(hexd_byte)  # appends xord byte to xord_buffers

    return "".join(xord_buffers)  # transform list of bytes to a string


def get_xord_hex_bytes_score(xord_hex_bytes, phrase):
    score = 0
    for p in range(0, len(phrase)):  # iterates through each character in the scoring phrase

        for byte in xord_hex_bytes:  # iterates through each byte in given xord_hex_bytes

            if phrase[p] == byte:  # if character equal to byte data

                score += p  # add score value of phrase character to score

    return score


def challenge_three(input_hex, phrase, answer):
    hex_bytes = bytes.fromhex(input_hex)  # gets hexadecimal string byte data

    results = []  # holds a tuple of the data, score, and key the data was xor'd with in a list data structure
    for key in range(0, 256):  # iterate through all possible byte values
        xord_hex_bytes = ''.join([chr(hex_byte ^ key) for hex_byte in hex_bytes])  # xor hex_bytes with key
        xord_hex_bytes_score = get_xord_hex_bytes_score(xord_hex_bytes, phrase)  # gets the byte score

        results.append((xord_hex_bytes, key, xord_hex_bytes_score))  # appends xord_hex_bytes, key, and score to results

    results = sorted(results, key=lambda x: x[2], reverse=True)  # sorts results by score

    for result in results:  # iterates through each result
        if result[0] == answer:  # checks results xord_hex_bytes against answer
            return True  # returns True if matches answer

    return False  # returns False if no matches


def challenge_four(input_file_path, phrase, answer):
    with open(input_file_path) as input_file:  # open input file to get challenge data
        lines = input_file.readlines()  # puts each line of input_file into a list data structure

    stripped_lines = [x.strip() for x in lines]  # strip all \n line endings from each line in content

    line_results = []  # used to hold the best result from each lines top scoring byte score

    for line in stripped_lines:  # for line in input file
        hex_bytes = bytes.fromhex(line)  # gets hexadecimal string byte data

        results = []  # holds a tuple of the data, score, and key the data was xor'd with in a list data structure

        for key in range(0, 256):  # iterates through all possible byte values
            xord_hex_bytes = ''.join([chr(hex_byte ^ key) for hex_byte in hex_bytes])  # xor hex_bytes with key
            xord_hex_bytes_score = get_xord_hex_bytes_score(xord_hex_bytes, phrase)  # gets the byte score

            results.append((xord_hex_bytes, key, xord_hex_bytes_score))  # appends a tuple of the bytes, key, and score

        results = sorted(results, key=lambda x: x[2], reverse=True)  # sorts the line by byte scores
        line_results.append(results[0])  # appends the top scoring result to line results

    line_results = sorted(line_results, key=lambda x: x[2], reverse=True)  # sorts line_results by top scoring result

    for result in line_results:  # iterate through each line
        if result[0] == answer:  # checks result against answer
            return True  # returns True if answer is found in line_results

    return False  # returns False if no matches
