from .challenges import challenge_one, challenge_two, challenge_three, challenge_four


#########################
# Set One Challenge One #
###############################################################
# Description: Encodes a given hexadecimal string into base64 #
###############################################################
answer_1 = "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"
input_hex_1 = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"


def test_challenge_one():
    assert challenge_one(input_hex_1) == answer_1


#########################
# Set One Challenge Two #
##################################################################################
# Description: Takes two equal length buffers and produces their XOR Combination #
##################################################################################
buffer_one = "1c0111001f010100061a024b53535009181c"
buffer_two = "686974207468652062756c6c277320657965"
answer_2 = "746865206b696420646f6e277420706c6179"


def test_challenge_two():
    assert challenge_two(buffer_one, buffer_two) == answer_2


###########################
# Set One Challenge Three #
####################################################################################
# Description: Takes a hex encoded string and find the XOR key it was encoded with #
####################################################################################
input_hex_3 = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"
phrase_3 = list('uldrhsnioatE')
answer_3 = 'Cooking MC\'s like a pound of bacon'


def test_challenge_three():
    assert challenge_three(input_hex_3, phrase_3, answer_3) is not False


##########################
# Set One Challenge Four #
######################################################################################################################
# Description:  Takes an input file with lines of hex strings and finds the line thats been XOR'd with a single byte #
######################################################################################################################
input_file_path_4 = "set_one/input_file_4.txt"
phrase_4 = list('uldrhsnioatE')
answer_4 = 'Now that the party is jumping\n'


def test_challenge_four():
    assert challenge_four(input_file_path_4, phrase_4, answer_4) is not False
